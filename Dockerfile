FROM ubuntu
MAINTAINER Nico Kreiling <nico.kreiling@gmail.com>

RUN apt-get update && \
    apt-get -y install \
        sudo  \
        bash \
        curl \
        python \
        jq \
        docker.io \
        python-pip 
RUN curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
RUN apt-get install -y nodejs

RUN pip install 'docker-compose==1.8.0'

COPY "./src" "/src"
WORKDIR /src
# RUN find /src -type f -name "*.sh" | xargs -L1 chmod +x

# Prepare Prequir
RUN cd loadCheatSheets && npm install .

CMD /src/loadCheatSheets/loadCheatSheets.sh
# CMD ["/bin/bash"]