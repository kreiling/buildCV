FROM mhart/alpine-node:latest
MAINTAINER Nico Kreiling <nico.kreiling@gmail.com>

RUN apk update && \
    apk upgrade && \
    apk add --update \
        bash \
        curl \
        python \
        docker \
        openrc \
        'py-pip==8.1.2-r0' --no-cache \
    && rm -rf /var/cache/apk/*

RUN pip install 'docker-compose==1.8.0'

COPY "./src" "/src"
WORKDIR /src

# Prepare Prequir
RUN cd loadCheatSheets && npm install .

CMD ["/bin/bash"]