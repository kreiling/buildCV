
## DEV
```sh
# Build Image
docker build -t cv-builder:latest .

# Run for development
docker run  -v $(pwd)/src:/src cv-builder /src/loadCheatSheets/loadCheatSheets.sh
docker run  -v $(pwd)/src:/src cv-builder /src/loadCheatSheets/loadCheatSheets.sh

# Run evaluate parser
docker run  -v $(pwd)/src:/src cv-builder /src/loadCheatSheets/evaluateParser.sh

# Run node-Skript in debug mode
node debug ./src/loadCheatSheets/index.js '/Users/nkreiling/Downloads/test.md'

```

### Test parser

```sh
mocha --reporter=nyan

node index.js $(pwd)/tests/test-document.md | jq '.'

# Locally analyze data
elasticdump --output=http://127.0.0.1:9209/cheatsheets/markdown --input=http://35.157.154.191:9200/cheatsheets --type=data
```


### Setup AWS

```sh
# run setupAws.sh and then register runner:
export token=Lrq1tNyewnWbDHYpRw2G

# shell
sudo gitlab-runner register -n \
  --tag-list aws,sh \
  --url https://gitlab.com/ \
  --registration-token $token \
  --executor shell \
  --description "AWS Shell gitlab-runner" 
  
sudo gitlab-runner register -n \
  --tag-list aws,docker \
  --url https://gitlab.com/ \
  --registration-token $token \
  --executor docker \
  --description "AWS docker-gitlab-runner" \
  --docker-image "docker:latest" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock \
  --docker-network-mode host \
  --docker-privileged

# Start elasticsearch
sudo sysctl -w vm.max_map_count=262144
docker run -d -p 9200:9200 -p 9300:9300 --name es krlng/es:cv

# Debug runner
docker run -it --privileged --net=host --name builder registry.gitlab.com/kreiling/buildcv bash
```