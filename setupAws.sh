export instanceID=$(aws ec2 run-instances --image-id "ami-5aee2235" --count 1 --instance-type t2.medium --key-name id_rsa --security-groups launch-wizard-1 |  jq '.Instances[0].InstanceId' --raw-output)
echo "Go to sleep to wait for AWS Instance"
sleep 120
echo "Go on.."
export publicIp=$(aws ec2 describe-instances --instance-ids $instanceID | jq '.Reservations[0].Instances[].PublicIpAddress' --raw-output)
# aws ec2 associate-address --instance-id $instanceID --allocation-id eipalloc-ff760996

# Install Docker
ssh ubuntu@${publicIp} << EOF
    sudo apt-get -y --no-install-recommends install curl  apt-transport-https  ca-certificates  curl  software-properties-common
    curl -fsSL https://apt.dockerproject.org/gpg | sudo apt-key add -
    sudo add-apt-repository \
       "deb https://apt.dockerproject.org/repo/ \
       ubuntu-$(lsb_release -cs) \
       main"
    sudo apt-get update
    
    # Install docker
    sudo apt-get -y install docker.io
    sudo gpasswd -a ${USER} docker
    sudo usermod -aG docker $(whoami)
EOF

# Install Docker runner
ssh ubuntu@${publicIp} << EOF
    curl -fsSL https://apt.dockerproject.org/gpg | sudo apt-key add -
    curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.deb.sh | sudo bash
    sudo apt-get -y install gitlab-runner
    sudo gpasswd -a gitlab-runner docker
    sudo usermod -aG docker gitlab-runner
EOF