export privateToken=$1
export url=$(git remote get-url origin)
curl --header "PRIVATE-TOKEN: ${privateToken}" "https://gitlab.inovex.de/api/v3/projects" \
| jq  '.[] | select(.ssh_url_to_repo == "${url}")'

echo "Get Project ID"
export id=$(curl --header "PRIVATE-TOKEN: ${privateToken}" "https://gitlab.inovex.de/api/v3/projects" | jq --arg url $(git remote get-url origin) '.[] | select(.ssh_url_to_repo == $url).id')

echo "GET Cluster password"
export clusterPw=$(curl --header "PRIVATE-TOKEN: ${privateToken}" "https://gitlab.inovex.de/api/v3/projects/${id}/variables" | jq '.[] | select(.key == "CLUSTER_PW").value'  | sed -e 's/^"//' -e 's/"//') 

# Start cluster
echo "Start Cluster"
gcloud container clusters create --password $clusterPw --disk-size=50 --cluster-version=1.5.2 kubigdata 

# Write 
echo "update Gitlab variables"
export ip=$(gcloud container clusters describe kubigdata | grep endpoint | sed -e 's/endpoint: //')
curl --request PUT --header "PRIVATE-TOKEN: ${privateToken}" "https://gitlab.inovex.de/api/v3/projects/${id}/variables/CI_K8S_APISERVER" --form "value=${ip}"

#gcloud container clusters delete -q kubigdata


aws configure