# Add CV Data
export ES=$1

curl -s -XDELETE ${ES}'/cv'
# Load Data from inca API
cat ./cv.json | jq -c '.projects' > projects.json
cat ./cv.json | jq -c '.basics' > basics.json
cat ./cv.json | jq -c '.work' > work.json
cat ./cv.json | jq -c '.education' > education.json
cat ./cv.json | jq -c '.volunteer' > volunteer.json

# Extract interesting data
sed 's/\}\,[ \t]{/\},\'$'\n{/g' projects.json | jq -c '.[] |{ index: { _index: "cv", _type: "projects"} },. ' > projects2.json
sed 's/\}\,[ \t]{/\},\'$'\n{/g' work.json | jq -c '.[] |{ index: { _index: "cv", _type: "work"} },. ' > work2.json
sed 's/\}\,[ \t]{/\},\'$'\n{/g' education.json | jq -c '.[] |{ index: { _index: "cv", _type: "education"} },. ' > education2.json
sed 's/\}\,[ \t]{/\},\'$'\n{/g' volunteer.json | jq -c '.[] |{ index: { _index: "cv", _type: "volunteer"} },. ' > volunteer2.json

curl -s -XPOST ${ES}'/cv/projects/_bulk' --data-binary @projects2.json
curl -s -XPOST ${ES}'/cv/basics/' --data-binary @basics.json
curl -s -XPOST ${ES}'/cv/work/_bulk' --data-binary @work2.json
curl -s -XPOST ${ES}'/cv/education/_bulk' --data-binary @education2.json
curl -s -XPOST ${ES}'/cv/volunteer/_bulk' --data-binary @volunteer2.json