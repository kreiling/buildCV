# Add CV Data
export ES=$1
export INCA_HOST=$2
export INCA_USER=$3
export INCA_PW=$4

# Load Data from inca API
export loginData='{"username":"'${INCA_USER}'","password":"'${INCA_PW}'","rememberMe":true}'
export token=$(curl ${INCA_HOST}'/_api/authenticate' -H 'Authorization: Basic bmtyZWlsaW5nOkJlYXJpbmcuMTY=' -H 'Origin: '${INCA_HOST} -H 'Accept-Encoding: gzip, deflate, br' -H 'Accept-Language: en-US,en;q=0.8,de;q=0.6' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36' -H 'Content-Type: application/json;charset=UTF-8' -H 'Accept: application/json, text/plain, */*' -H 'Referer: https://inca.inovex.de/' -H 'Cookie: _ga=GA1.2.1211380720.1465820575' -H 'Connection: keep-alive' --data-binary $loginData --compressed | jq .'id_token' --raw-output) 
curl ${INCA_HOST}'/_api/employee/'${INCA_USER} -H 'Authorization: Basic bmtyZWlsaW5nOkJlYXJpbmcuMTY=' -H 'Accept-Encoding: gzip, deflate, sdch, br' -H 'Accept-Language: en-US,en;q=0.8,de;q=0.6' -H 'Upgrade-Insecure-Requests: 1' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36' -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8' -H 'Cookie: _ga=GA1.2.1211380720.1465820575; X-INCA-AUTHORIZATION='${token} -H 'If-None-Match: "0346858d99c4903de13b358e95ecb251a"' -H 'Connection: keep-alive' --compressed | jq -c '.skills' > skills.json

# Extract interesting data
sed 's/\}\,[ \t]{/\},\'$'\n{/g' skills.json | jq -c '.[] |{ index: { _index: "cv", _type: "skill"} },. ' > skills2.json

curl -XPOST ${ES}'/cv/skill/_bulk' --data-binary @skills2.json

cat cv.json| jq -c '.skills' > skills.json