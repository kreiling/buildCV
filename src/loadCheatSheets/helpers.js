exports.flatten = function flatten(arr, obj) {
    if (obj.children === null) {
        arr.push(obj);
    } else {
        obj.children.forEach(child => {
            arr = flatten(arr, child);
        })
        return arr;
    }
    return arr;
}

exports.checkHeading = function checkHeading(tag) {
    const arr = ["h1","h2","h3","h4","h5","h6","heading"]
    if (arr.indexOf(tag) >=0) {
        return true
    } else {
        return false
    }
}

exports.getTag = function getTag(str) {
    switch((str.match(/#/g) || []).length) {
        case 1:
            return "h1"
            break;
        case 2:
            return "h2"
            break;
        case 3:
            return "h3"
            break;
        default:
            if((str.match(/\*\*/g) || []).length) {
                return "header"
            }
            if((str.match(/__/g) || []).length) {
                return "header"
            }
    }
}

class Source {
    constructor(info, full, code, comment) {
        this.info = info.trim(),
        this.full = full.trim(),
        this.code = code.trim(),
        this.codeOneLine = code.replace(/\n/g, '; ').trim(),
        this.comment = comment.replace(/#/g, '').trim()
    }
}

exports.parseCode = function (obj, sign) {
    results = [];
    full = obj.content;
    // Get the amount of comments
    count = (full.match(/#/g) || []).length

    // no comments: Just append
    if (count === 0) {
        results.push(new Source("sh",full, full,""))
    }

    // one comment: Detect position to decide what is comment, what is content
    else if (count === 1) {
        debugger;
        // # sh \n something
        if (full.indexOf("#") == 0) {
            splitted = full.split("\n");
            const comment = splitted[0];
            const singleLine = splitted.slice(1).join(";")
            const code = splitted.slice(1).join("\n")
            results.push(new Source("sh", full, code, comment,singleLine))
        } else {
            splitted = full.split("#");
            const comment = splitted[1];
            const code = splitted[0]
            results.push(new Source("sh", full, code, comment))
        }
    }
    // multiple comments: Split into single lines and check what is comment, what is content
    else {
        debugger;
        let comment_prefix = "",
         code_prefix = "",
         code = "",
         comment = ""
         // trim whitespaces in final line
         full = full.replace(/^(?=\n)$|^\s*|\s*$|\n\n+/gm,"")
        full.split("\n").forEach(line => {
            splitted = line.split("#");
            if (splitted[1] != '' && splitted[1] != undefined)
                comment = comment_prefix.trim() + " " + splitted[1].trim();
            if (splitted[0] != '' && splitted[0] != undefined)
                code = code_prefix.trim() + " " + splitted[0].trim()
            if (code === "") {
                comment_prefix = comment;
            } else if (comment === comment_prefix && code_prefix === "") {
                code_prefix = code;
            } else {
                results.push(new Source("sh", comment + "\n"+code, code, comment))
            }
        })
    }
    return results;
}

exports.splitBlocks = function splitBlocks(arr) {
    const newArr = [];
    arr.forEach(obj => {
        let splitted;
        if (obj.tag === "code" && obj.info === "sh") {
            splitted = obj.content.split(/\n\n|\n#/);
        } else {
            splitted = obj.content.split("\n\n");
        }
        if (splitted.length > 1) {
            splitted.forEach(content => {
                const copy = Object.assign({}, obj, {
                    "content": content
                });
                newArr.push(copy);
            })
        } else {
            newArr.push(obj)
        }
    })
    return newArr;
}