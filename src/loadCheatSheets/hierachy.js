var checkHeading = require('./helpers.js').checkHeading;
class hierachyStore {
    constructor() {
        this.h1 = undefined
        this.h2 = undefined
        this.h3 = undefined
        this.h4 = undefined
        this.h5 = undefined
        this.h6 = undefined
        this.heading = undefined
        this.end;
    }

    add(tag, value) {
        // console.log("Tag",tag,value)
        if (checkHeading(tag)) {        
            this.heading = undefined
            if (tag <="h5")
                this.h6 = undefined
            if (tag <="h4") 
                this.h5 = undefined
            if (tag <="h3") 
                this.h4 = undefined
            if (tag <="h2") 
                this.h3 = undefined
            if (tag <="h1") 
                this.h2 = undefined
            this[tag] = value.replace(/#/g, '').replace(/\*/g, '-').replace(/_/g, "");
            this.end = this[tag]
        }
        else {
            throw "add not defined for tag "+tag
        }
    }

    get() {
        let arr = []
        for (var property in this) {
            if (this[property] && property !== "end") {
                arr.push(this[property])
            }
        }
        return arr
    }
    getEnd() {
        return this.end
    }
    getAnchor() {
        let anchor = undefined
        let i=6
        while (!anchor && i > 0){
            anchor = this["h"+i]
            i=i-1;
        }
        if (anchor)
            return anchor.replace(/ /g, '-').replace(/\//g, "").toLowerCase();
        else
            return undefined
    }
}
store = new hierachyStore();
module.exports = store;