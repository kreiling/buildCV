var MarkdownIt = require('markdown-it');
var fs = require('fs');
var q = require('q');
var md = new MarkdownIt();
var promises = [];
var request = require('request');

var getTag = require('./helpers.js').getTag;
var flatten = require('./helpers.js').flatten;
var splitBlocks = require('./helpers.js').splitBlocks;
var checkHeading = require('./helpers.js').checkHeading;
var hierachy = require('./hierachy.js');
var parseCode = require('./helpers.js').parseCode;

class esEntry {
	constructor(file, type, tag, section, link, src, hierachy) {
		this.hierachy = hierachy
		this.meta = {
			file: file,
			type: type,
			tag: tag,
			section: section
		}
		this.src = src
		this.link = link

		if (this.src.code) {
			this.meta.type = "code"
		}
	}
}

// Main Job
function processFile(filepath) {
	return new Promise((resolve, reject) => {
		let filename = filepath.split("/")[filepath.split("/").length - 1]
		let tag, type, heading, link = "",
			headers = [],
			skipped = 0,
			all = []

		// Read Markdown File
		fs.readFile(filepath, 'utf8', function read(err, data) {
			if (err) {
				throw err;
				reject();
			}
			try {
				// parse markdown files
				var result = md.parse(data, {}).forEach(el => {
					// Split Bigger Blocks if there are spare blank lines between
					let els = splitBlocks(flatten([], el));
					els.forEach(el => {
						// For opening & closing tags
						// Remove opening and closing tags while keeping their information add them as attributes to the actual content-elements
						if (el.type.includes('_open') || el.type.includes('_close')) {
							skipped = skipped + 1;
							let types = el.type.split("_");

							// set tag when opened
							if (types[1] === "open") {
								type = types[0]
								// Handle Headings with spaces (## Title)
								tag = getTag(el.markup)
							}
							// reset tag on closing tags
							if (types[1] === "close") {
								type = undefined;
								tag = undefined;
							}
						}

						// For actual content blocks
						else {
							let codeBlocks, section;
							// If Heading do similar this as for opening/closing Tags: store content to add attributes to content elements
							if (type) {
								// Handle Headings without spaces (##Title)
								if (! tag){
									tag = getTag(el.content)
								}
								if (checkHeading(tag)) {
									hierachy.add(tag,el.content)
									section = el.content;
									link = filename + "#" + hierachy.getAnchor();
								}
							}
							// Special Treatment for Shell Scripts
							if (el.tag === "code") {
								codeBlocks = parseCode(el, "#")
									// Add the previously extracted content information to the content Elements
								codeBlocks = codeBlocks.map(src => {
										return new esEntry(
											filename,
											type,
											tag,
											section,
											link,
											src,
											hierachy.get()
										);
									})
									// use spread-syntax to add all elements to one array
								all.push(...codeBlocks)
							}
						}
					})
				});
			} catch (e) {
				console.error(filepath + ":", e)
			}
			resolve(all);
		})
	});
}

function main() {
	// Remove dummy parameters
	params = process.argv.slice(2);
	// Check if filenames to convert have been passed
	if (params.length == 0) {
		console.log("please name the files which shall be converted");
	}

	// Start conversion process for all files
	params.forEach(function(val, index, array) {
		// Process File
		promises.push(processFile(val));
	});

	// After all files got processed, concat them into an array for output
	q.all(promises).then(x => {
		debugger;
		let flattend = [].concat.apply([], x);
		console.log(JSON.stringify(flattend));
	});
}

main();