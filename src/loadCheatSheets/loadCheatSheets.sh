# Add CheatSheet Data
export ES=${1:-http://nico.kreiling.family}
export MD_DIR=${2:-/tmp}

# Create Index
echo "> delete old index"
curl --silent --request DELETE ${ES}':9200/cheatsheets'               
# curl --request PUT '${ES}/cheatsheets' -d @schema.json

# Git Clone 
echo "> clone project"
echo "> Write code to ES: "$ES
if [[ $MD_DIR == /tmp ]]; then 
    git clone https://gitlab.com/kreiling/cheatsheets ${MD_DIR};
fi

rm errors.log 2> /dev/null
for f in ${MD_DIR}/*.md; do 
    rm error.log 2> /dev/null
    node $(dirname $0)/index.js $f 2>> error.log 1> raw.json

    # add meta-information for ES-bulkload
    sed 's/\}\,[ \t]{/\},\'$'\n{/g' raw.json | jq -c '.[] | { index: { _index: "cheatsheets", _type: "markdown"} }, . ' > bulkFormat.json 2>> error.log

    # Fill Index
    curl --silent --request POST ${ES}':9200/cheatsheets/markdown/_bulk' --data-binary @bulkFormat.json  > result.log 2>> error.log

    if [ ! -s error.log ]
    then
        echo "SUCCESS: " $f ":" $(wc -l < bulkFormat.json)
    else
        echo "ERROR: " $f ":" $(wc -l < bulkFormat.json)
        cat ./error.log
        cat error.log >>./errors.log
    fi;
done;


if [ ! -s errors.log ]
then
    echo "Cheatsheets contains" $(curl --silent --request GET ${ES}':9200/cheatsheets/_count' | jq ".count") "documents"
    echo "> successfull finished"
    echo "> successfull finished"
    exit 0
else
    echo "> finished with errors"
    exit 1
fi;