// Load the assertion library
var childProcess = require('child_process'),
  chai = require('chai'),
  should = chai.should(),
  assert = chai.assert;

describe('E2E Tests', function() {
  let result;

  beforeEach(function(done) {
    path = __dirname.split("/").slice(0, -1).join("/")
    var child = require('child_process').execFile('node', [path + '/index.js', __dirname + "/test-document.md"], function(err, stdout, stderr) {
      // Node.js will invoke this callback when the 
      if (err) {
        throw err
      } else {
        result = JSON.parse(stdout.toString());
        done();
      }
    });
  });

  // This should pass
  it('correct amount of elements', function() {
    result.length.should.equal(5);
  });

  it('Single code header', function() {
    let obj = {
      "hierachy": ["Title", "Subheading"],
      "meta": {
        "file": "test-document.md",
        "type": "code"
      },
      "src": {
        "info": "sh",
        "full": "# Single code line\nls /",
        "code": "ls /",
        "codeOneLine": "ls /",
        "comment": "Single code line"
      },
      "link": "test-document.md#subheading"
    }
    result[0].should.deep.equal(obj);
  });

  it('Single code appended', function() {
    let obj = {
      "hierachy": ["Title", "Subheading"],
      "meta": {
        "file": "test-document.md",
        "type": "code"
      },
      "src": {
        "info": "sh",
        "full": "ls / # Single code line",
        "code": "ls /",
        "codeOneLine": "ls /",
        "comment": "Single code line"
      },
      "link": "test-document.md#subheading"
    }
    result[1].should.deep.equal(obj);
  });

  it('multiline code header', function() {
    let obj = {
      "hierachy": ["Title", "Subheading"],
      "meta": {
        "file": "test-document.md",
        "type": "code"
      },
      "src": {
        "info": "sh",
        "full": "# for loop \nfor a in ls .\ndo\n  echo $ab\ndone",
        "code": "for a in ls .\ndo\n  echo $ab\ndone",
        "codeOneLine": "for a in ls .; do;   echo $ab; done",
        "comment": "for loop"
      },
      "link": "test-document.md#subheading"
    }
    result[2].should.deep.equal(obj);
  });

  it('complex 1', function() {
    let obj = {
      "hierachy": ["Title", "Subheading"],
      "meta": {
        "file": "test-document.md",
        "type": "code"
      },
      "src": {
        "info": "sh",
        "full": "List all files\nls -la",
        "code": "ls -la",
        "codeOneLine": "ls -la",
        "comment": "List all files"
      },
      "link": "test-document.md#subheading"
    }
    result[3].should.deep.equal(obj);
  });

  it('complex 2', function() {
    let obj = {
      "hierachy": ["Title", "Subheading"],
      "meta": {
        "file": "test-document.md",
        "type": "code"
      },
      "src": {
        "info": "sh",
        "full": "List with list view\nls -l",
        "code": "ls -l",
        "codeOneLine": "ls -l",
        "comment": "List with list view"
      },
      "link": "test-document.md#subheading"
    }
    result[4].should.deep.equal(obj);
  });
});

describe('Get Tag', function() {
  var getTag = require('../helpers.js').getTag;

  it('h1', function() {
    getTag("#Something").should.equal("h1")
  });

  it('h2', function() {
    getTag("##Subheading").should.equal("h2")
  });

  it('h3', function() {
    getTag("###Test").should.equal("h3")
  });

  it('header', function() {
    getTag("__test__").should.equal("header")
    getTag("**test**").should.equal("header")
  });
});


describe('Hierachy Store', function() {
  const hierachy = require('../hierachy.js');

  it('add h1 and h2', function() {
    hierachy.add("h1", "Test")
    hierachy.add("h2", "Test2")
    hierachy.get().should.eql(["Test", "Test2"])
    hierachy.getEnd().should.eql("Test2")
    hierachy.getAnchor().should.eql("test2")
  });

  it('add h3', function() {
    hierachy.add("h3", "Test3")
    hierachy.get().should.eql(["Test", "Test2", "Test3"])
    hierachy.getAnchor().should.eql("test3")
  });

  it('change h3', function() {
    hierachy.add("h3", "Test3b")
    hierachy.get().should.eql(["Test", "Test2", "Test3b"])
    hierachy.getAnchor().should.eql("test3b")
  });

  it('change h2', function() {
    hierachy.add("h2", "Test2b")
    hierachy.get().should.eql(["Test", "Test2b"])
    hierachy.getAnchor().should.eql("test2b")
  });

  it('add heading', function() {
    hierachy.add("heading", "TestHeader")
    hierachy.get().should.eql(["Test", "Test2b", "TestHeader"])
    hierachy.getAnchor().should.eql("test2b")
  });

  it('change heading', function() {
    hierachy.add("heading", "TestHeader2")
    hierachy.get().should.eql(["Test", "Test2b", "TestHeader2"])
    hierachy.getAnchor().should.eql("test2b")
  });

  it('add h1', function() {
    hierachy.add("h1", "Test1b")
    hierachy.get().should.eql(["Test1b"])
    hierachy.getAnchor().should.eql("test1b")
  });
});