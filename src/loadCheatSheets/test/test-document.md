# Title

##Subheading

__Some bold text above__

```sh
# Single code line
ls /

ls / # Single code line

# for loop 
for a in ls .
do
  echo $ab
done

# List
ls
-la # all files
-l # with list view
```

__Shortcuts__

Shortcut  | Command
------------- | -------------
/  | Search
\  | Filter

### Other List

* __Point1__: desc1
* __Point2__: desc2

### Nested List:

* main1
	* sub1
	* sub2
* main2